package docs

import (
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
)

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/exchange/history exchange historyRequest
// Получение истории цен (данных много, может подвесить браузер).
// security:
//   - Bearer: []
// responses:
//   200: ExchangeListResponse

// swagger:route GET /api/1/exchange/min exchange minPricesRequest
// Получение информации о минимальных ценах.
// security:
//   - Bearer: []
// responses:
//   200: ExchangeResponse

// swagger:route GET /api/1/exchange/max exchange maxPricesRequest
// Получение информации о максимальных ценах.
// security:
//   - Bearer: []
// responses:
//   200: ExchangeResponse

// swagger:route GET /api/1/exchange/average exchange averagePricesRequest
// Получение информации о средних ценах.
// security:
//   - Bearer: []
// responses:
//   200: ExchangeResponse

// swagger:route GET /api/1/exchange/ticker exchange tickerRequest
// Получение данных тикера.
// security:
//   - Bearer: []
// responses:
//   200: ExchangeResponse

// swagger:response ExchangeListResponse
type ExchangeListResponse struct {
	// in:body
	Body messages.ExchangeListResponse
}

// swagger:response ExchangeResponse
type ExchangeResponse struct {
	// in:body
	Body messages.ExchangeResponse
}
