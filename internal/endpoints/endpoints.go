package endpoints

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/modules"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/middleware"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)

			r.Route("/exchange", func(r chi.Router) {
				exchController := controllers.Exchange
				r.Use(authCheck.CheckStrict)
				r.Get("/ticker", exchController.Ticker)
				r.Get("/history", exchController.History)
				r.Get("/min", exchController.MinPrices)
				r.Get("/max", exchController.MaxPrices)
				r.Get("/average", exchController.AveragePrices)
			})
		})
	})

	return r
}
