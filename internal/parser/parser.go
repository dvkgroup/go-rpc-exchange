package parser

import (
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/models"
	"go.uber.org/zap"
	"net/http"
)

const (
	tickerURL         = "https://api.exmo.com/v1.1/ticker"
	tickerContentType = "application/x-www-form-urlencoded"
)

type RatesParser struct {
	decoder godecoder.Decoder
	logger  *zap.Logger
}

func NewRatesParser(decoder godecoder.Decoder, logger *zap.Logger) *RatesParser {
	return &RatesParser{decoder: decoder, logger: logger}
}

func (r *RatesParser) Do() models.Exchange {
	resp, err := http.Post(tickerURL, tickerContentType, nil)
	if err != nil {
		return nil
	}

	var result models.Exchange

	err = r.decoder.Decode(resp.Body, &result)
	if err != nil {
		r.logger.Error("parser: parse ticker err", zap.Error(err))
		return models.Exchange{}
	}

	return result
}
