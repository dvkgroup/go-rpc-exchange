package storages

import (
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/modules/exch/storage"
	"gitlab.com/dvkgroup/go-rpc-extension/db/adapter"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/cache"
)

type Storages struct {
	Exchange storage.ExchangeStorager
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Exchange: storage.NewExchangeStorage(sqlAdapter, cache),
	}
}
