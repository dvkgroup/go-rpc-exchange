package modules

import (
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/modules/exch/service"
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/parser"
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/storages"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/transport/amqp"
)

type Services struct {
	Exchange interfaces.Exchanger
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	parser := parser.NewRatesParser(components.Decoder, components.Logger)
	rabbit := amqp.NewAMQPProducer(components.Conf.RabbitMQ, components.Logger)

	return &Services{
		Exchange: service.NewExchangeService(storages.Exchange, components.Logger, parser, rabbit),
	}
}
