package service

import (
	"context"
	"fmt"
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/modules/exch/core"
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/modules/exch/storage"
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/parser"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
	"gitlab.com/dvkgroup/go-rpc-extension/models"
	"gitlab.com/dvkgroup/go-rpc-extension/transport/amqp"
	"go.uber.org/zap"
)

const (
	offsetToUpdate = 100
)

type ExchangeService struct {
	storage storage.ExchangeStorager
	logger  *zap.Logger
	parser  *parser.RatesParser
	core    *core.ExchangeCore
	rabbit  *amqp.AMQPProducer
}

func NewExchangeService(storage storage.ExchangeStorager, logger *zap.Logger, parser *parser.RatesParser, rabbit *amqp.AMQPProducer) *ExchangeService {
	core := core.NewExchangeCore()

	return &ExchangeService{storage: storage, logger: logger, parser: parser, core: core, rabbit: rabbit}
}

func (s *ExchangeService) Create(ctx context.Context, in messages.ExchangeCreateIn) messages.ExchangeOut {
	pricesMin := s.GetMinList(ctx)
	if pricesMin.ErrorCode != 0 {
		return messages.ExchangeOut{
			ErrorCode: pricesMin.ErrorCode,
		}
	}

	pricesMax := s.GetMaxList(ctx)
	if pricesMax.ErrorCode != 0 {
		return messages.ExchangeOut{
			ErrorCode: pricesMax.ErrorCode,
		}
	}

	dto := in.Data.ToDTO()

	for i := range dto {
		// сверяем с последним запросом
		// если Updated совпадает, пропускаем
		if dto[i].Updated == s.core.LastPrices[dto[i].PairName].Updated {
			continue
		}

		// записываем в базу
		exchangeID, err := s.storage.Create(ctx, dto[i])

		// если записи в базе Min нет, то добовляем ее
		if v, ok := pricesMin.Data[dto[i].PairName]; !ok {
			s.storage.CreateMin(ctx, models.ExchangeMinDTO{
				PairName:   dto[i].PairName,
				ExchangeID: exchangeID,
			})
		} else { // иначе обновляем
			if dto[i].BuyPrice < (v.BuyPrice - offsetToUpdate) {
				s.storage.UpdateMin(ctx, models.ExchangeMinDTO{
					PairName:   dto[i].PairName,
					ExchangeID: exchangeID,
				})

				// отправка сообщения в rabbit
				message := fmt.Sprintf("Новая цена %s:%f (min)", dto[i].PairName, dto[i].BuyPrice)
				s.rabbit.SendMessage([]byte(message))
			}
		}

		// если записи в базе Max нет, то добовляем ее
		if v, ok := pricesMax.Data[dto[i].PairName]; !ok {
			s.storage.CreateMax(ctx, models.ExchangeMaxDTO{
				PairName:   dto[i].PairName,
				ExchangeID: exchangeID,
			})
		} else { // иначе обновляем
			if dto[i].BuyPrice > (v.BuyPrice + offsetToUpdate) {
				s.storage.UpdateMax(ctx, models.ExchangeMaxDTO{
					PairName:   dto[i].PairName,
					ExchangeID: exchangeID,
				})

				// отправка сообщения в rabbit
				message := fmt.Sprintf("Новая цена %s:%f (max)", dto[i].PairName, dto[i].BuyPrice)
				s.rabbit.SendMessage([]byte(message))
			}
		}

		if err != nil {
			return messages.ExchangeOut{
				ErrorCode: errors.ExchangeServiceGeneralErr,
			}
		}
	}

	s.core.SetLastPrices(in.Data)

	return messages.ExchangeOut{
		Success: true,
	}
}

func (s *ExchangeService) GetTicker(ctx context.Context) messages.ExchangeOut {
	rates := s.parser.Do()

	res := s.Create(ctx, messages.ExchangeCreateIn{Data: rates})

	return messages.ExchangeOut{
		Success:   res.Success,
		ErrorCode: res.ErrorCode,
		Data:      rates,
	}
}

// GetList возвращает всю истории
func (s *ExchangeService) GetList(ctx context.Context) messages.ExchangeListOut {
	list, err := s.storage.GetList(ctx)
	if err != nil {
		return messages.ExchangeListOut{
			ErrorCode: errors.ExchangeServiceGeneralErr,
		}
	}

	out := messages.ExchangeListOut{
		Success: true,
		Data:    make(models.ExchangeList, len(list)),
	}

	for i := range list {
		pairName, value := list[i].ToExchange()
		out.Data[pairName] = append(out.Data[pairName], value)
	}

	return out
}

// GetMaxList возвращает  список пар криптовалют с максимальной ценой
func (s *ExchangeService) GetMaxList(ctx context.Context) messages.ExchangeOut {
	list, err := s.storage.ListMax(ctx)
	if err != nil {
		return messages.ExchangeOut{
			ErrorCode: errors.ExchangeServiceGeneralErr,
		}
	}

	ids := make([]int64, 0, len(list))

	for i := range list {
		ids = append(ids, list[i].ExchangeID)
	}

	listByIDs, err := s.storage.GetByIDs(ctx, ids)
	if err != nil {
		return messages.ExchangeOut{
			ErrorCode: errors.ExchangeServiceGeneralErr,
		}
	}

	out := messages.ExchangeOut{
		Success: true,
		Data:    make(models.Exchange, len(listByIDs)),
	}

	for i := range listByIDs {
		pairName, value := listByIDs[i].ToExchange()
		out.Data[pairName] = value
	}

	return out
}

// GetMinList возвращает  список пар криптовалют с минимальной ценой
func (s *ExchangeService) GetMinList(ctx context.Context) messages.ExchangeOut {
	list, err := s.storage.ListMin(ctx)
	if err != nil {
		return messages.ExchangeOut{
			ErrorCode: errors.ExchangeServiceGeneralErr,
		}
	}

	ids := make([]int64, 0, len(list))

	for i := range list {
		ids = append(ids, list[i].ExchangeID)
	}

	listByIDs, err := s.storage.GetByIDs(ctx, ids)
	if err != nil {
		return messages.ExchangeOut{
			ErrorCode: errors.ExchangeServiceGeneralErr,
		}
	}

	out := messages.ExchangeOut{
		Success: true,
		Data:    make(models.Exchange, len(listByIDs)),
	}

	for i := range listByIDs {
		pairName, value := listByIDs[i].ToExchange()
		out.Data[pairName] = value
	}

	return out
}

func (s *ExchangeService) GetAverageList(ctx context.Context) messages.ExchangeOut {
	max := s.GetMaxList(ctx)
	min := s.GetMinList(ctx)

	if max.ErrorCode != 0 {
		return messages.ExchangeOut{
			Data:      nil,
			ErrorCode: max.ErrorCode,
		}
	}

	if min.ErrorCode != 0 {
		return messages.ExchangeOut{
			Data:      nil,
			ErrorCode: min.ErrorCode,
		}
	}

	out := messages.ExchangeOut{
		Success: true,
		Data:    make(models.Exchange, len(max.Data)),
	}

	for k, v := range max.Data {
		out.Data[k] = models.ExchangeValue{
			BuyPrice:  (v.BuyPrice + min.Data[k].BuyPrice) / 2,
			SellPrice: (v.SellPrice + min.Data[k].SellPrice) / 2,
			LastTrade: v.LastTrade,
			High:      v.High,
			Low:       v.Low,
			Avg:       v.Avg,
			Vol:       v.Vol,
			VolCurr:   v.VolCurr,
			Updated:   v.Updated,
		}
	}

	return out
}
