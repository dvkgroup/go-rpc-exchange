package controller

import (
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/responder"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
	"net/http"
)

type Exchanger interface {
	Ticker(http.ResponseWriter, *http.Request)
	History(http.ResponseWriter, *http.Request)
	MinPrices(w http.ResponseWriter, r *http.Request)
	MaxPrices(w http.ResponseWriter, r *http.Request)
	AveragePrices(w http.ResponseWriter, r *http.Request)
}

type Exchange struct {
	service interfaces.Exchanger
	responder.Responder
	godecoder.Decoder
}

func NewExchange(service interfaces.Exchanger, components *component.Components) Exchanger {
	return &Exchange{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (e *Exchange) Ticker(w http.ResponseWriter, r *http.Request) {
	data := e.service.GetTicker(r.Context())

	e.OutputJSON(w, messages.ExchangeResponse{
		Success:   data.Success,
		ErrorCode: data.ErrorCode,
		Data:      data.Data,
	})
}

func (e *Exchange) History(w http.ResponseWriter, r *http.Request) {
	data := e.service.GetList(r.Context())

	e.OutputJSON(w, messages.ExchangeListResponse{
		Success:   true,
		ErrorCode: data.ErrorCode,
		Data:      data.Data,
	})
}

func (e *Exchange) MinPrices(w http.ResponseWriter, r *http.Request) {
	data := e.service.GetMinList(r.Context())

	e.OutputJSON(w, messages.ExchangeResponse{
		Success:   true,
		ErrorCode: data.ErrorCode,
		Data:      data.Data,
	})
}

func (e *Exchange) MaxPrices(w http.ResponseWriter, r *http.Request) {
	data := e.service.GetMaxList(r.Context())

	e.OutputJSON(w, messages.ExchangeResponse{
		Success:   true,
		ErrorCode: data.ErrorCode,
		Data:      data.Data,
	})
}

func (e *Exchange) AveragePrices(w http.ResponseWriter, r *http.Request) {
	data := e.service.GetMaxList(r.Context())

	e.OutputJSON(w, messages.ExchangeResponse{
		Success:   true,
		ErrorCode: data.ErrorCode,
		Data:      data.Data,
	})
}
