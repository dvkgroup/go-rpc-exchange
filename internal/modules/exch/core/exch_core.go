package core

import "gitlab.com/dvkgroup/go-rpc-extension/models"

type ExchangeCore struct {
	LastPrices models.Exchange
}

func NewExchangeCore() *ExchangeCore {
	return &ExchangeCore{LastPrices: make(models.Exchange)}
}

func (e *ExchangeCore) SetLastPrices(last models.Exchange) *ExchangeCore {
	e.LastPrices = last
	return e
}

func (e *ExchangeCore) GetLastPrices() *models.Exchange {
	return &e.LastPrices
}
