package storage

import (
	"context"
	"github.com/Masterminds/squirrel"
	"gitlab.com/dvkgroup/go-rpc-extension/db/adapter"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/cache"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/db/scanner"
	"gitlab.com/dvkgroup/go-rpc-extension/models"
)

type ExchangeStorage struct {
	adapter *adapter.SQLAdapter
	cache   cache.Cache
}

const (
	exchangeCacheKey     = "exch:%d"
	exchangeCacheTTL     = 15
	exchangeCacheTimeout = 50
)

func NewExchangeStorage(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *ExchangeStorage {
	return &ExchangeStorage{adapter: sqlAdapter, cache: cache}
}

func (s *ExchangeStorage) Create(ctx context.Context, e models.ExchangeDTO) (int64, error) {
	id, err := s.adapter.Create(ctx, &e)

	return int64(id), err
}

func (s *ExchangeStorage) Update(ctx context.Context, e models.ExchangeDTO) error {
	err := s.adapter.Update(ctx, &e, adapter.Condition{
		Equal: squirrel.Eq{
			"id": e.ID,
		},
	}, scanner.Update)

	return err
}

// GetByIDs - получение всех записей из БД
func (s *ExchangeStorage) GetList(ctx context.Context) ([]models.ExchangeDTO, error) {
	var list []models.ExchangeDTO
	err := s.adapter.List(ctx, &list, "exchange", adapter.Condition{})
	if err != nil {
		return nil, err
	}

	return list, nil
}

// GetByIDs - получение записей по ID из БД
func (s *ExchangeStorage) GetByIDs(ctx context.Context, ids []int64) ([]models.ExchangeDTO, error) {
	var list []models.ExchangeDTO
	err := s.adapter.List(ctx, &list, "exchange", adapter.Condition{
		Equal: map[string]interface{}{
			"id": ids,
		},
	})
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (s *ExchangeStorage) ListMax(ctx context.Context) ([]models.ExchangeMaxDTO, error) {
	var list []models.ExchangeMaxDTO

	err := s.adapter.List(ctx, &list, "exchange_max", adapter.Condition{})

	return list, err
}

func (s *ExchangeStorage) CreateMax(ctx context.Context, e models.ExchangeMaxDTO) error {
	_, err := s.adapter.Create(ctx, &e)

	return err
}

func (s *ExchangeStorage) UpdateMax(ctx context.Context, e models.ExchangeMaxDTO) error {
	err := s.adapter.Update(ctx, &e, adapter.Condition{
		Equal: squirrel.Eq{
			"pair_name": e.PairName,
		},
	}, scanner.Update)

	return err
}

func (s *ExchangeStorage) ListMin(ctx context.Context) ([]models.ExchangeMinDTO, error) {
	var list []models.ExchangeMinDTO

	err := s.adapter.List(ctx, &list, "exchange_min", adapter.Condition{})

	return list, err
}

func (s *ExchangeStorage) CreateMin(ctx context.Context, e models.ExchangeMinDTO) error {
	_, err := s.adapter.Create(ctx, &e)

	return err
}

func (s *ExchangeStorage) UpdateMin(ctx context.Context, e models.ExchangeMinDTO) error {
	err := s.adapter.Update(ctx, &e, adapter.Condition{
		Equal: squirrel.Eq{
			"pair_name": e.PairName,
		},
	}, scanner.Update)

	return err
}
