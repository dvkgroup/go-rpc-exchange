package storage

import (
	"context"
	"gitlab.com/dvkgroup/go-rpc-extension/models"
)

type ExchangeStorager interface {
	Create(context.Context, models.ExchangeDTO) (int64, error)
	Update(context.Context, models.ExchangeDTO) error
	GetList(ctx context.Context) ([]models.ExchangeDTO, error)
	GetByIDs(ctx context.Context, ids []int64) ([]models.ExchangeDTO, error)

	ListMin(ctx context.Context) ([]models.ExchangeMinDTO, error)
	CreateMin(context.Context, models.ExchangeMinDTO) error
	UpdateMin(ctx context.Context, e models.ExchangeMinDTO) error

	ListMax(ctx context.Context) ([]models.ExchangeMaxDTO, error)
	CreateMax(context.Context, models.ExchangeMaxDTO) error
	UpdateMax(ctx context.Context, e models.ExchangeMaxDTO) error
}
