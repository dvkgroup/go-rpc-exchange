package modules

import (
	"gitlab.com/dvkgroup/go-rpc-exchange/internal/modules/exch/controller"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
)

type Controllers struct {
	Exchange controller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	exchangeController := controller.NewExchange(services.Exchange, components)

	return &Controllers{
		Exchange: exchangeController,
	}
}
