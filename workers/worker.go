package workers

import (
	"context"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"go.uber.org/zap"
	"time"
)

const (
	// exchange parse interval
	exchangeParseInterval = 10 * time.Second
)

type Worker struct {
	service interfaces.Exchanger
	logger  *zap.Logger
}

func NewWorker(service interfaces.Exchanger, logger *zap.Logger) *Worker {
	return &Worker{service: service, logger: logger}
}

func (r *Worker) Run() {
	r.logger.Info("start ticker parser")

	ctx := context.TODO()

	go func() {
		ticker := time.NewTicker(exchangeParseInterval)
		defer ticker.Stop()

		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				r.service.GetTicker(ctx)
				r.logger.Info("ticker parser: success")
			}
		}
	}()
}
